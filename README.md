# :hospital: APP Health

O APP Health é um sistema de software médico para gerenciar o dia a dia de clínicas e consultórios médicos.

# :bookmark_tabs: Avaliação

Adicionar uma nova opção no menu lateral que redirecione para uma nova tela, e seja possível realizar um CRUD de Sujeitos de atenção

## Funcionalidades da aplicação

- ***`Listar os sujeitos de atenção:`*** Deve criar uma tabela com uma lista com os sujeitos de atenção cadastrados no sistema.
- ***`Adicionar novos sujeitos de atenção:`*** Deve ser possível adicionar novos sujeitos de atenção, contendo os seguintes campos: `Nome`, `Data de nascimento`, `CPF`, ` Telefone principal`, `Email` e `Município`.

- ***`Visualizar e editar sujeitos cadastrados:`*** Deve ser possível visualizar as informações de um sujeito cadastro e editar o seu cadastro.

### Observações

- As telas novas devem ser responsivas, de maneira que atendam um notebook pequeno. (width mínimo de 1024px).
- Validar os campos que sejam possíveis.
- Acrescentar máscara nos campos que necessitarem.
- Já existe uma query para buscar os Municípios;
- Onde adicionar as novas telas, se deverá ou não adicionar novas bibliotecas, essas e outras decisões durante o desenvolvimento, é com você.

### O que será avaliado?

- Entrega das atividades citadas acima;
- Lógica aplicada na resolução dos problemas.
- Estilização das novas telas criadas.
- A qualidade e padronização do código.

# :rocket: Iniciar ambiente

- ***Importante:*** Poderá encontrar problemas em executar o projeto com versões mais recentes do Node. Recomendamos que utilize a versão 12 do node. Sugerimos a 12.18.4

Execute `node -v` para verificar a versão instalada em sua máquina.

### Comandos para iniciar o desenvolvimento

Baixar as dependencias:

`yarn`

Iniciar servidor de desenvolvimento:

`yarn start`

### Credenciais

- login: amadeu
- senha: admin

### GraphQL

O GraphQL é uma ferramenta que permite a comunicação com o servidor de backend. Aqui na pasta do projeto contém um arquivo chamado ********GraphQL.pdf********, ele contém um tutorial de como se conectar e executar suas primeiras queryes (requisições ao backend).