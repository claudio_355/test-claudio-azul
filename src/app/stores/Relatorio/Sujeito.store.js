import { action, observable } from "mobx";
import Api from "../../config/api";
import string from "../../utils/string";
import Moment from "moment";

const sujeitoAtencaoDefault = {
  nome: "",
  cpf: "",
  dataNascimento: "",
  id: "",
  contato: {
    email: "",
    telefonePrincipal: "",
  },
  endereco: {
    municipio: {
      nome: "",
      uf: "",
    },
  },
};
const defaultSearchDTO = {
  pageNumber: 0,
  pageSize: 20,
  search: "",
};
export default class SujeitoAtencaoStore {
  usuarioStore = null;
  @observable currentPage = null;
  @observable totalElements = 0;
  @observable numberOfElements = 0;
  @observable loading = true;
  @observable saving = false;
  @observable opening = false;
  @observable open = false;
  @observable sujeitoAtencaoList = [];
  @observable sujeitoAtencao = {
    ...sujeitoAtencaoDefault,
  };
  @observable searchDTO = {
    ...defaultSearchDTO,
  };

  @action async findAll() {
    try {
      if (this.currentPage === this.searchDTO.pageNumber) {
        return;
      }

      this.currentPage = this.searchDTO.pageNumber;

      this.loading = true;

      const response = await Api.post("", {
        query: `
                        query{
                                findAllSujeitoAtencao(searchDTO:{search:"${this.searchDTO.search}"pageNumber: ${this.searchDTO.pageNumber}}){
                                    totalElements
                                    numberOfElements
                                    content{
                                    id
                                    nome
                                    cpf
                                    dataNascimento
                                    contato{
                                      email
                                      telefonePrincipal
                                    }
                                    endereco{
                                      municipio{
                                        nome
                                        uf
                                    }
                                }
                            }
                        }
                    }         
                    `,
      });

      const list = response.data.data.findAllSujeitoAtencao.content;

      this.numberOfElements =
        response.data.data.findAllSujeitoAtencao.numberOfElements;
      this.searchDTO.pageNumber += 1;
      if (
        this.numberOfElements === 0 &&
        string.isEmpty(this.searchDTO.search)
      ) {
        return;
      }
      if (this.searchDTO.pageNumber > 1) {
        this.sujeitoAtencaoList = [...this.sujeitoAtencaoList, ...list];
      } else {
        this.sujeitoAtencaoList = [...list];
      }
    } catch (error) {
      throw error;
    } finally {
      this.loading = false;
    }
  }
  @action openNew() {
    this.open = true;
    this.sujeitoAtencao = {
      ...sujeitoAtencaoDefault,
    };
  }
  @action async save(sujeitoAtencao) {
    console.log(sujeitoAtencao);
    try {
      if (this.saving) {
        return;
      }

      if (!sujeitoAtencao) {
        throw new Error("Preencha os dados");
      }
      let metodo = "create";
      if (sujeitoAtencao && sujeitoAtencao.id) {
        metodo = "update";
      }

      if (
        string.isEmpty(sujeitoAtencao.nome) ||
        string.isEmpty(sujeitoAtencao.cpf) ||
        string.isEmpty(sujeitoAtencao.dataNascimento)
      ) {
        throw new Error("Preencha os dados");
      }

      const sujeitoAtencaoDados = {
        nome: sujeitoAtencao.nome,
        cpf: sujeitoAtencao.cpf,
        dataNascimento: Moment(sujeitoAtencao.dataNascimento).format(
          "YYYY-MM-DD"
        ),
        email: sujeitoAtencao.contato.email,
        telefonePrincipal: sujeitoAtencao.contato.telefonePrincipal,
        id: sujeitoAtencao.id,
      };
      let idf = "";
      if (metodo === "update") {
        idf = `, id: "${sujeitoAtencaoDados.id}"`;
      }

      console.log(
        sujeitoAtencaoDados.nome,
        sujeitoAtencaoDados.cpf,
        sujeitoAtencaoDados.dataNascimento,
        sujeitoAtencaoDados.telefonePrincipal
      );
      this.saving = true;
      await Api.post("", {
        query: `
        mutation{
            ${metodo}SujeitoAtencao(sujeitoAtencao:{ nome:"${sujeitoAtencaoDados.nome}",
               cpf: "${sujeitoAtencaoDados.cpf}",
                dataNascimento : "${sujeitoAtencaoDados.dataNascimento}",
                 contato:{telefonePrincipal:"${sujeitoAtencaoDados.telefonePrincipal}"}${idf}}) {
               id
            }
        }
      `,
      });
    } catch (error) {
      throw error;
    } finally {
      this.saving = false;
    }
  }

  @action closeModal() {
    this.open = false;
    this.sujeitoAtencao = {
      ...sujeitoAtencaoDefault,
    };
  }
  @action edit(id) {
    this.open = true;
    this.loadById(id);
  }
  @action async loadById(id) {
    try {
      this.opening = true;
      const response = await Api.post("", {
        query: `
          query{
            findByIdSujeitoAtencao(id: "${id}") {
                  id
                  nome
                  dataNascimento
                  cpf
                  contato{
                    telefonePrincipal
                  }
                  endereco {
                    municipio{
                      nome
                      uf
                    }
                  }
              }
          }
      `,
      });

      const data = response.data.data.findByIdSujeitoAtencao;

      if (data.endereco === null) {
        data.endereco = { municipio: { nome: "" } };
      }
      if (data.contato === null) {
        data.contato = { telefonePrincipal: "" };
      }
      this.sujeitoAtencao = { ...data };
    } catch (error) {
      throw error;
    } finally {
      this.opening = false;
    }
  }
}
