import React from "react";
import MaskedInput from "react-text-mask";



export const CPFMask = props => {
    const { inputRef, ...other } = props;
  
    return (
      <MaskedInput
        ref={ref => {
          inputRef(ref ? ref.inputElement : null);
        }}
        placeholderChar={"\u2000"}
        showMask={props.showmask}
        mask={[/\d/, /\d/, /\d/,".", /\d/, /\d/, /\d/,".", /\d/, /\d/,/\d/,"-",/\d/,/\d/]}
        {...other}
      />
    );
  };