import React from "react";
import { TextField as TextFieldDefault } from "@material-ui/core";
import { CEPMask } from "../Input/InputCEPForm";
import { CurrencyMask } from "../Input/InputCurrency";
import {PhoneMask} from "../Input/InputPhoneForm";
import {CPFMask} from "../Input/CPFMask";
const getCEPMaskProps = props => ({
  ...props,
  InputProps: {
    ...props.InputProps,
    inputComponent: CEPMask
  }
});
const getCPFMaskProps = props => ({
  ...props,
  InputProps: {
    ...props.InputProps,
    inputComponent: CPFMask
  }
});

const getCurrencyMaskProps = props => ({
  ...props,
  InputProps: {
    ...props.InputProps,
    inputComponent: CurrencyMask
  }
});
const getPhoneMask = props => ({
  ...props,
  InputProps: {
    ...props.InputProps,
    inputComponent: PhoneMask
  }
});

const getMaskProps = ({ withCEPMask, withCurrencyMask, withPhoneMask,withCPFMask, ...others }) => {
  if (withCEPMask) {
    return getCEPMaskProps(others);
  }
  if(withCPFMask){
    return getCPFMaskProps(others)
  }
  
  if (withCurrencyMask) {
    return getCurrencyMaskProps(others);
  }
  if (withPhoneMask) {
    return getPhoneMask(others);
  }

  return null;
};

const TextField = props => {
  const others = {
    ...props,
  };
  
  
  /**
   * Remove as propriedades que não são permitidas no TextFied do MaterialUI
   */
  delete others.withCEPMask;
  delete others.withCurrencyMask;
  delete others.withPhoneMask;
  delete others.withCPFMask;
  return (
    <TextFieldDefault
      variant="outlined"
      fullWidth
      {...others}
      {...getMaskProps(props)}
      mask={value => (value ? [/\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/] : [])}
    />
  );
};

export default TextField;
