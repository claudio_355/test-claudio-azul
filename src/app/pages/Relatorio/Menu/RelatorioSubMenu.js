import React from "react";

import Grid from "@material-ui/core/Grid";
import ItemSubMenu from "./itemSubMenu";
import { withRouter } from "react-router-dom";

class RelatorioSubMenu extends React.PureComponent {
  render() {
    const { match, location } = this.props;

    return (
      <Grid container direction={"column"} wrap={"nowrap"}>
        <ItemSubMenu
          to={match.url}
          name={"Pesquisar"}
          location={location}
        />

      </Grid>
    );
  }
}

export default withRouter(RelatorioSubMenu);
