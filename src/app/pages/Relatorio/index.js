import { withStyles } from "@material-ui/core/styles";
import React from "react";
import ReactDOM from "react-dom";
import PanelLeft from "../../components/PanelLeft/PanelLeft";
import { Grid } from "@material-ui/core";
import Header from "../../template/Header/Header";
import styles from "../../assets/jss/pages/relatoriosStyle";
import Route from "react-router-dom/Route";
import Pesquisar from "./Pesquisar/Pesquisar";
import RelatorioSubMenu from "./Menu/RelatorioSubMenu";

class Relatorio extends React.Component {
  render() {
    const { classes, match } = this.props;
    return (
      <div className={classes.root}>
        <PanelLeft className={classes.panelLeft}>
          <Grid item>
            <Header>
              <Grid
                item
                container
                justify={"center"}
                xs={12}
                alignItems={"center"}
              >
                <h3 className={classes.titleHeader}>Relatório</h3>
              </Grid>
            </Header>
          </Grid>
          <RelatorioSubMenu />
        </PanelLeft>
        <Route exact path={match.path} component={Pesquisar} />
      </div>
    );
  }
}
export default withStyles(styles)(Relatorio);
