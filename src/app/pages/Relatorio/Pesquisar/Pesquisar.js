import styles from "../../../assets/jss/pages/pesquisaStyle";
import { Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import Header from "../../../template/Header/Header";
import React from "react";
import ReactDOM from "react-dom";
import Profile from "../../../template/Header/Profile";
import InputSearch from "../../../components/Input/Input";
import GridItem from "../../../components/GridItem";
import Table from "../../../components/Table/Table";
import Scroll from "../../../components/InfiniteScroll/Scroll";
import { inject, observer } from "mobx-react";
import debounce from "lodash.debounce";
import SpeedDials from "../../../components/SpeedDials";
import PesquisarModal from "./PesquisarModal";
import AddIcon from "@material-ui/icons/Add";
import moment from "moment";
import string from "../../../utils/string";
const columns = [
  {
    Header: "Nome",
    getValue: (row) => {
      return row.nome;
    },
    props: { left: "true" },
  },
  {
    Header: "CPF",
    getValue: (row) => {
      return string.formatCPF(row.cpf);
    },
    props: { left: "true" },
  },
  {
    Header: "data de Nascimento",
    getValue: (row) => {
      if (!moment(row.dataNascimento).isValid()) {
        return "---";
      }
      return moment(row.dataNascimento).format("DD/MM/YYYY");
    },
    props: { left: "true" },
  },
  {
    Header: "Municipio",
    getValue: (row) => {
      if (row.endereco === null || row.endereco.municipio === null) {
        return "----";
      }

      return row.endereco.municipio.nome;
    },
    props: { left: "true" },
  },
  {
    Header: "Telefone",
    getValue: (row) => {
      if (row.contato === null || row.contato.telefonePrincipal === null) {
        return "----";
      }

      return string.formatPhone(row.contato.telefonePrincipal);
    },
    props: { left: "true" },
  },
];

@inject("sujeitoAtencaoStore")
@observer
class Pesquisar extends React.Component {
  constructor(props) {
    super(props);

    this.debounceConsulta = debounce(this.debounceConsulta, 500);
  }
  componentDidMount() {
    const { sujeitoAtencaoStore } = this.props;

    sujeitoAtencaoStore.findAll({ pageNumber: 0 });
  }

  handleClickAction = () => {
    const { sujeitoAtencaoStore } = this.props;

    sujeitoAtencaoStore.openNew();
  };

  handleItemClick = (id) => {
    const { sujeitoAtencaoStore } = this.props;
    console.log(id);
    sujeitoAtencaoStore.edit(id);
  };
  handleSearchChange = async (e) => {
    const { sujeitoAtencaoStore } = this.props;
    const search = e.target.value;
    sujeitoAtencaoStore.currentPage = null;
    sujeitoAtencaoStore.searchDTO.pageNumber = 0;
    sujeitoAtencaoStore.searchDTO.search = search;
    this.debounceConsulta();
  };
  debounceConsulta() {
    const { sujeitoAtencaoStore } = this.props;

    sujeitoAtencaoStore.findAll();
  }
  loadMore = () => {
    const { sujeitoAtencaoStore } = this.props;

    sujeitoAtencaoStore.findAll();
  };

  render() {
    const { classes, sujeitoAtencaoStore } = this.props;
    const {
      searchDTO,
      numberOfElements,
      sujeitoAtencaoList,
      loading,
      open,
    } = sujeitoAtencaoStore;
    const hasMore = numberOfElements > 0;
    const actions = [
      {
        icon: <AddIcon />,
        name: "Adicionar",
      },
    ];

    return (
      <div className={classes.content}>
        <Header padding>
          <Grid item xs={8}>
            <h3 className={classes.titleHeader}>
              Pesquisar paciente de interesse
            </h3>
          </Grid>
          <Grid item xs={8}>
            <Grid container justify={"space-between"} alignItems={"center"}>
              <Grid item />
              <Grid item>
                <Profile />
              </Grid>
            </Grid>
          </Grid>
        </Header>

        <div className={classes.tableContainer}>
          <div className={classes.search}>
            <GridItem xs={8}>
              <InputSearch
                onChange={this.handleSearchChange}
                value={searchDTO.search}
                placeholder="Pesquisar"
              />
            </GridItem>
          </div>

          {!loading && sujeitoAtencaoList.length === 0 && (
            <div className={classes.notFoundContainer}>
              <h3>Nenhum item encontrado</h3>
            </div>
          )}
          {sujeitoAtencaoList.length > 0 && (
            <Scroll
              loadMore={this.loadMore}
              hasMore={hasMore}
              pageStart={0}
              className={classes.scrollContainer}
            >
              <Table
                dados={sujeitoAtencaoList}
                columns={columns}
                clickable={true}
                handleClick={this.handleItemClick}
              />
            </Scroll>
          )}
        </div>
        <SpeedDials actions={actions} onClickAction={this.handleClickAction} />

        <PesquisarModal open={open}></PesquisarModal>
      </div>
    );
  }
}
export default withStyles(styles)(Pesquisar);
