import React from "react";
import Dialog from "../../../components/Dialog/Dialog";
import DialogHeader from "../../../components/Dialog/DialogHeader";
import SujeitoAtencaoStore from "../../../stores/Relatorio/Sujeito.store";
import { inject, observer } from "mobx-react";
import { withStyles } from "@material-ui/core/styles/index";
import DialogContent from "@material-ui/core/DialogContent";
import Grid from "@material-ui/core/Grid";
import { TextField } from "../../../components/TextField";
import { Typography, CircularProgress } from "@material-ui/core";
import ButtonClearPrimary from "../../../components/Button/ButtonClearPrimary";
import ButtonPrimary from "../../../components/Button/ButtonPrimary";
import string from "../../../utils/string";
import moment from "moment";
import { PhoneMask } from "../../../components/Input/InputPhoneForm";
import InputDateForm from "../../../components/Input/InputDateForm";
const styles = () => ({
  root: {
    display: "grid",
    gridTemplateRows: "auto 1fr",
  },
  formGroup: {
    marginTop: 15,
  },
  formActions: {
    marginTop: 20,
  },
  inlineButtons: {
    paddingLeft: "15px",
    display: "inline-flex",
    width: "100%",
  },
  inlineInput: {
    display: "inline-flex",
    justifyContent: "space-between",
    width: "100%",
  },
});
@inject("sujeitoAtencaoStore")
@observer
class PesquisarModal extends React.Component {
  handleChange = (field, event) => {
    const { sujeitoAtencaoStore } = this.props;

    if (field === "email") {
      sujeitoAtencaoStore.sujeitoAtencao.contato[field] = event.target.value;
    } else if (field === "telefonePrincipal") {
      let valor = event.target.value;
      let stringValor = valor
        .replace(/-/g, "")
        .replace(/ /g, "")
        .replace("(", "")
        .replace(")", "");
      sujeitoAtencaoStore.sujeitoAtencao.contato[field] = stringValor;
    } else if (field === "cpf") {
      let valor = event.target.value;
      let stringValor = valor.replace(/-/g, "").replace(/\./g, "");
      sujeitoAtencaoStore.sujeitoAtencao[field] = stringValor;
    } else if (field === "dataNascimento") {
      sujeitoAtencaoStore.sujeitoAtencao[field] = moment(event).format(
        "YYYY-MM-DD"
      );
    } else {
      sujeitoAtencaoStore.sujeitoAtencao[field] = event.target.value;
    }
  };
  handleConfirmar = async (e) => {
    const { sujeitoAtencaoStore } = this.props;
    console.log(
      moment(
        sujeitoAtencaoStore.sujeitoAtencao.dataNascimento,
        "YYYY-MM-DD",
        true
      )
    );
    if (
      !string.validaCPF(sujeitoAtencaoStore.sujeitoAtencao.cpf) &&
      sujeitoAtencaoStore.sujeitoAtencao.cpf !== ""
    ) {
      window.alert("CPF Invalido");
      return;
    }

    try {
      const { sujeitoAtencaoStore } = this.props;

      const sujeitoAtencao = {
        ...sujeitoAtencaoStore.sujeitoAtencao,
      };

      await sujeitoAtencaoStore.save(sujeitoAtencao);
      sujeitoAtencaoStore.closeModal();
      sujeitoAtencaoStore.sujeitoAtencaoList = [];
      sujeitoAtencaoStore.currentPage = null;
      sujeitoAtencaoStore.searchDTO.pageNumber = 0;
      sujeitoAtencaoStore.findAll();
    } catch (error) {
      console.warn(error && error.message);
    }
  };
  handleClose = () => {
    const { onClose, sujeitoAtencaoStore } = this.props;

    sujeitoAtencaoStore.closeModal();

    if (typeof onClose === "function") {
      onClose();
    }
  };

  render() {
    const { classes, open, sujeitoAtencaoStore } = this.props;
    const { sujeitoAtencao, saving, opening } = sujeitoAtencaoStore;
    console.log(sujeitoAtencao);
    return (
      <Dialog open={open} maxWidth={"sm"} fullWidth={true}>
        <div className={classes.root}>
          <DialogHeader
            title={"Sujeito Atenção"}
            closeButton={true}
            actionClose={this.handleClose}
          />
          <DialogContent>
            <Grid item xs={12} className={classes.formGroup}>
              <Typography color="primary" component="label">
                Nome
              </Typography>
              <TextField
                value={sujeitoAtencao.nome || ""}
                onChange={(e) => this.handleChange("nome", e)}
              />
            </Grid>
            <div className={classes.inlineInput}>
              <Grid item xs={6} className={classes.formGroup}>
                <Typography color="primary" component="label">
                  CPF
                </Typography>
                <TextField
                  value={sujeitoAtencao.cpf || ""}
                  onChange={(e) => this.handleChange("cpf", e)}
                  withCPFMask
                />
              </Grid>
              <Grid item xs={4} className={classes.formGroup}>
                <Typography color="primary" component="label">
                  Data de Nascimento
                </Typography>
                <InputDateForm
                  variant="outlined"
                  fullWidth
                  openTo="month"
                  views={["year", "month", "day"]}
                  value={sujeitoAtencao.dataNascimento || ""}
                  onChange={(e) => this.handleChange("dataNascimento", e)}
                  format="DD/MM/YYYY"
                  placeholder={"__/__/____"}
                  mask={(value) =>
                    value
                      ? [
                          /\d/,
                          /\d/,
                          "/",
                          /\d/,
                          /\d/,
                          "/",
                          /\d/,
                          /\d/,
                          /\d/,
                          /\d/,
                        ]
                      : []
                  }
                />
              </Grid>
            </div>
            <div className={classes.inlineInput}>
              <Grid item xs={12} className={classes.formGroup}>
                <Typography color="primary" component="label">
                  E-mail
                </Typography>
                <TextField
                  value={sujeitoAtencao.contato.email || ""}
                  onChange={(e) => this.handleChange("email", e)}
                />
              </Grid>
              <Grid item xs={8} className={classes.formGroup}>
                <Typography color="primary" component="label">
                  Telefone
                </Typography>
                <TextField
                  value={sujeitoAtencao.contato.telefonePrincipal || ""}
                  onChange={(e) => this.handleChange("telefonePrincipal", e)}
                  withPhoneMask
                />
              </Grid>
            </div>

            <Grid item xs={12} className={classes.formActions}>
              <Grid container>
                <Grid item xs={6} />
                <Grid item xs={3} className={classes.inlineButtons}>
                  <ButtonClearPrimary onClick={this.handleClose}>
                    Cancelar
                  </ButtonClearPrimary>
                </Grid>
                <Grid item xs={3} className={classes.inlineButtons}>
                  <ButtonPrimary type="submit" onClick={this.handleConfirmar}>
                    Confirmar
                    {saving === true && (
                      <CircularProgress
                        color="inherit"
                        size={14}
                        style={{ marginLeft: 10 }}
                      />
                    )}
                  </ButtonPrimary>
                </Grid>
              </Grid>
            </Grid>
          </DialogContent>
        </div>
      </Dialog>
    );
  }
}

export default withStyles(styles)(PesquisarModal);
